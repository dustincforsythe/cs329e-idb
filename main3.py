"""
main3.py
This program uses tools such as Flask and SQLAlchemy to implement the book database.

"""


from flask import Flask, render_template
from models import app, db, Book, Author, Publisher
from create_db import create_books, create_authors, create_publishers
from flask_sqlalchemy import SQLAlchemy 
import os,subprocess
from operator import itemgetter



app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']=os.environ.get("DB_STRING",'postgres://postgres:pGsQl.42!!@localhost:5432/bookdb')
db = SQLAlchemy(app)

@app.route('/') 
def index():
	"""
	index() renders the splash page.
	"""
	return render_template('Splash.html')

@app.route('/book/') 
def book():
	"""
	book() renders the dynamic book titles page, which has a sortable, paginated, searchable table of book titles.
	"""
	books = db.session.query(Book).all()
	#return render_template('Titles.html', books = books)
	return render_template('TitlesDynamic.html', books = books)


#@app.route('/book2/') 
#def book():
 #	books = db.session.query(Book).all()
  #  return render_template('Titles.html', books = books)





@app.route('/Authors/')
def Authors():
	"""
	Authors() renders the dynamic authors titles page, which has a sortable, paginated, searchable table of authors.
	"""
	authors = db.session.query(Author).all()
	return render_template('AuthorsDynamic.html', authors =authors)






@app.route('/Books/')
def Titles():
	"""
	Titles() renders the dynamic book titles page, which has a sortable, paginated, searchable table of book titles.
	"""
	books = db.session.query(Book).all()
	#return render_template('Titles.html', books = books)
	return render_template('TitlesDynamic.html',books = books)


@app.route('/Publishers/')
def Publishers():
	"""
	Publishers() renders the dynamic publishers page, which has a sortable, paginated, searchable table of publishers.
	"""
	publishers = db.session.query(Publisher).all()
	return render_template('PublishersDynamic.html', publishers = publishers)


@app.route('/BookDetails/<BookName>')
def BookDetails(BookName):
	"""
	BookDetails() renders the the dynamic info page for a particular book. The template
	autopopulates with the necessary information depending on the book selected by the user.
	"""
	book_ind = str(BookName)
	books = db.session.query(Book).all()
	for i in books:
		print(type(i.booknum))
		if (book_ind == str(i.booknum)):
			return render_template('BookDetails.html', books = books, i =i)
			
@app.route('/AuthorDetails/<AuthorName>')
def AuthorDetails(AuthorName):
	"""
	AuthorDetails() renders the the dynamic info page for a particular author. The template
	autopopulates with the necessary information depending on the author selected by the user.
	"""
	author_ind = str(AuthorName)
	authors = db.session.query(Author).all()
	for i in authors:
		if (author_ind == str(i.authornum)):
			return render_template('AuthorDetails.html', authors = authors, i = i)

@app.route('/PublisherDetails/<PublisherName>')
def PublisherDetails(PublisherName):
	"""
	PublisherDetails() renders the the dynamic info page for a particular publisher. The template
	autopopulates with the necessary information depending on the publisher selected by the user.
	"""
	publisher_ind = str(PublisherName)
	publishers = db.session.query(Publisher).all()
	for i in publishers:
		if (publisher_ind == str(i.publishernum)):
			return render_template('PublisherDetails.html', publishers = publishers, i = i)


@app.route('/About/')
def About():
	"""
	About() renders the about page, full of information about the developers and technical information
	about the project.
	"""
	return render_template('About.html')

@app.route('/test/')
def test():
	"""
	test() renders the test page, which contains the results of running the unit tests.
	"""
	p = subprocess.Popen(["python", "test.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output=err+out
	output = output.decode("utf-8") #convert from byte type to string type
	
	return render_template('test.html', output = " <br>".join(output.split("\n")))

@app.route('/dropdown/')
def dropdown():
	"""
	dropdown() allows the user to use drop down menus, like when selecting how many
	entries should be shown on each page.
	"""
	return render_template('dropdown.css', dropdown =dropdown)




if __name__ == "__main__":
	app.run()
# end of main3.py