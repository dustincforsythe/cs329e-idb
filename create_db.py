# beginning of create_db.py
import json
from models import db, Book, Author, Publisher

def create_dictionaries():
    book = load_json('books.json')
    AuthorsDict = {}
    PublishersDict = {}
    BooksDict = {}
    A=0
    P=0
    B = 0
    for oneBook in book: 
        author_entry = parse_info(oneBook,"authors","name")
        publisher_entry = parse_info(oneBook,"publishers","name")
        book_entry = oneBook['title']
        B = B + 1
        BooksDict[book_entry] = B
        if (author_entry not in AuthorsDict.keys()):
            A = A + 1
            AuthorsDict[author_entry] = A
        if (publisher_entry not in PublishersDict.keys()):
            P = P + 1
            PublishersDict[publisher_entry] = P
    return AuthorsDict, PublishersDict, BooksDict



def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file) 
        file.close()
    return jsn

def parse_info(onebook,st,st2):
    lst=onebook[st]
    st_info=lst[0]
    return st_info[st2]

    

def create_books(AuthorsDict, PublishersDict, BooksDict):
    book = load_json('books.json')
    for oneBook in book: 
        author = parse_info(oneBook,"authors","name")
        authornum = AuthorsDict[author]
        publisher = parse_info(oneBook,"publishers","name")
        publishernum = PublishersDict[publisher]
        title = oneBook['title']
        booknum = BooksDict[title]
        
        if 'subtitle' in oneBook.keys():
            subtitle=oneBook['subtitle']
        else:
            subtitle = 'N/A'
        if 'publication_date' in oneBook.keys():
            publication_date= oneBook['publication_date']
        else:
            publication_date = 'N/A'
        if 'isbn' in oneBook.keys():
            isbn= oneBook['isbn']
        else:
            isbn = 'N/A'
        if 'google_id' in oneBook.keys():
            google_id=oneBook['google_id']
        else:
            google_id = 'N/A'
        if 'image_url' in oneBook.keys():
            image_url = oneBook['image_url']
        else:
            image_url = 'https://econnect.baxter.com/assets/images/products/Renal/thumb_image_not_available.png'
        if 'description' in oneBook.keys():
            description=oneBook['description']
        else:
            description = 'N/A'
    
        newBook = Book(booknum = booknum, authornum = authornum, publishernum = publishernum, title = title, subtitle = subtitle, author=author,publisher=publisher, publication_date=publication_date, isbn = isbn, google_id = google_id, image_url = image_url, description = description)
        assert (newBook.booknum == booknum and newBook.authornum == authornum and newBook.publishernum == publishernum and newBook.title == title and newBook.subtitle == subtitle and newBook.author==author and newBook.publisher==publisher and newBook.publication_date==publication_date and newBook.isbn == isbn and newBook.google_id == google_id and newBook.image_url == image_url and newBook.description == description)
        # After I create the book, I can then add it to my session. 
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()

def create_authors(AuthorsDict, PublishersDict, BooksDict):
    book = load_json('books.json')
    authorslist= []
    for oneBook in book:
        authorsdict = oneBook['authors']
        author= parse_info(oneBook,"authors","name")
        if author not in authorslist:
            authorslist.append(author)
            
            authornum = AuthorsDict[author]
            publisher=parse_info(oneBook,"publishers","name")
            publishernum = PublishersDict[publisher]
            title = oneBook['title']
            booknum = BooksDict[title]
            
            if 'born' in authorsdict[0].keys():
                born = parse_info(oneBook, "authors", "born")
            else:
                born = 'N/A'
            if 'died' in authorsdict[0].keys():
                died = parse_info(oneBook, "authors", "died")
            else:
                died = 'N/A'
            if 'nationality' in authorsdict[0].keys():
                nationality = parse_info(oneBook, "authors", "nationality")
            else:
                nationality = 'N/A'
            if 'education' in authorsdict[0].keys():
                education = parse_info(oneBook, "authors", "education")
            else:
                education = 'N/A'
            if 'alma_mater' in authorsdict[0].keys():
                alma_mater = parse_info(oneBook, "authors", "alma_mater")
            else:
                alma_mater = 'N/A'
            if 'wikipedia_page' in authorsdict[0].keys():
                wikipedia_page = parse_info(oneBook, "authors", "wikipedia_url")
            else:
                wikipedia_page = 'N/A'
            if 'image_url' in authorsdict[0].keys():
                image_url = parse_info(oneBook, "authors", "image_url")
            else:
                image_url = 'https://econnect.baxter.com/assets/images/products/Renal/thumb_image_not_available.png'
            if 'description' in authorsdict[0].keys():
                description = parse_info(oneBook, "authors", "description")
            else:
                description = 'N/A'
        
    
    
    
            newAuthor = Author(booknum = booknum, authornum = authornum, publishernum = publishernum, author =author, title = title, publisher=publisher, born = born, died = died, nationality=nationality, education =education, alma_mater = alma_mater, wikipedia_page=wikipedia_page, image_url = image_url, description = description)
            assert (newAuthor.booknum == booknum and newAuthor.authornum == authornum and newAuthor.publishernum == publishernum and newAuthor.author ==author and newAuthor.title == title and newAuthor.publisher==publisher and newAuthor.born == born and newAuthor.died == died and newAuthor.nationality==nationality and newAuthor.education ==education and newAuthor.alma_mater == alma_mater and newAuthor.wikipedia_page==wikipedia_page and newAuthor.image_url == image_url and newAuthor.description == description)
            # After I create the book, I can then add it to my session. 
            db.session.add(newAuthor)
            # commit the session to my DB.
            db.session.commit()

def create_publishers(AuthorsDict, PublishersDict, BooksDict):
    book = load_json('books.json')
    publisherslist = []
    for oneBook in book:
        publishersdict = oneBook['publishers']
        publisher= parse_info(oneBook,"publishers","name")
        if publisher not in publisherslist:
            publisherslist.append(publisher)
            title = oneBook['title']
            booknum = BooksDict[title]
            author= parse_info(oneBook,"authors","name")
            authornum = AuthorsDict[author]
            publishernum = PublishersDict[publisher]
            if 'parent company' in publishersdict[0].keys():
                parent_company=parse_info(oneBook,"publishers","parent company")
            else:
                parent_company = 'N/A'
            if 'owner' in publishersdict[0].keys():
                owner=parse_info(oneBook,"publishers","owner")
            else:
                owner = 'N/A'
            if 'location' in publishersdict[0].keys():
                location=parse_info(oneBook,"publishers","location")
            else:
                location = 'N/A'
            if 'founded' in publishersdict[0].keys():
                founded=parse_info(oneBook,"publishers","founded")
            else:
                founded = 'N/A'
            
            
            if 'wikipedia_url' in publishersdict[0].keys():
                wikipedia_page_=parse_info(oneBook,"publishers","wikipedia_url")
            else:
                wikipedia_page_ = 'N/A'
            if 'description' in publishersdict[0].keys():
                description=parse_info(oneBook,"publishers","description")
            else:
                description = 'N/A'
            if 'website' in publishersdict[0].keys():
                website=parse_info(oneBook,"publishers","website")
            else:
                website = 'N/A'
            if 'image_url' in publishersdict[0].keys():
                image_url =parse_info(oneBook,"publishers","image_url")
            else:
                image_url = 'https://econnect.baxter.com/assets/images/products/Renal/thumb_image_not_available.png'
    
    
    
            newPublisher = Publisher(booknum = booknum, authornum = authornum, publishernum = publishernum, publisher = publisher, parent_company = parent_company, owner = owner, location =location, founded = founded, title = title, author=author, wikipedia_page_ = wikipedia_page_, description = description, website=website, image_url = image_url)
            assert (newPublisher.booknum == booknum and newPublisher.authornum == authornum and newPublisher.publishernum == publishernum and newPublisher.publisher == publisher and newPublisher.parent_company == parent_company and newPublisher.owner == owner and newPublisher.location ==location and newPublisher.founded == founded and newPublisher.title == title and newPublisher.author==author and newPublisher.wikipedia_page_ == wikipedia_page_ and newPublisher.description == description and newPublisher.website==website and newPublisher.image_url == image_url)
            # After I create the book, I can then add it to my session. 
            db.session.add(newPublisher)
            # commit the session to my DB.
            db.session.commit()

AuthorsDict, PublishersDict, BooksDict = create_dictionaries()      
create_books(AuthorsDict, PublishersDict, BooksDict)
create_authors(AuthorsDict, PublishersDict, BooksDict)
create_publishers(AuthorsDict, PublishersDict, BooksDict)
# end of create_db.py