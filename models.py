# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt). 

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy 
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']=os.environ.get("DB_STRING",'postgres://postgres:pGsQl.42!!@localhost:5432/bookdb')
db = SQLAlchemy(app)

class Book(db.Model): 
  __tablename__ = 'books'
  booknum = db.Column(db.String(), nullable = False)
  authornum = db.Column(db.String(), nullable = False)
  publishernum = db.Column(db.String(), nullable = False)
  title = db.Column(db.String(), primary_key= True)
  subtitle=db.Column(db.String(), nullable = True)
  author=db.Column(db.String(), nullable = True)
  publisher=db.Column(db.String(), nullable = True)
  publication_date=db.Column(db.String(), nullable = True)
  isbn = db.Column(db.String(), nullable = True)
  google_id =db.Column(db.String(), nullable = True)
  image_url=db.Column(db.String(), nullable = True)
  description=db.Column(db.String(), nullable = True)

class Author(db.Model):
  __tablename__ = 'authors'
  booknum = db.Column(db.String(), nullable = False)
  authornum = db.Column(db.String(), nullable = False)
  publishernum = db.Column(db.String(), nullable = False)
  author=db.Column(db.String(), nullable = True)
  title = db.Column(db.String(), primary_key= True)
  publisher=db.Column(db.String(), nullable = True)
  born=db.Column(db.String(), nullable = True)
  died=db.Column(db.String(), nullable = True)
  nationality=db.Column(db.String(), nullable = True)
  education=db.Column(db.String(), nullable = True)
  alma_mater=db.Column(db.String(), nullable = True)
  wikipedia_page=db.Column(db.String(), nullable = True)
  image_url=db.Column(db.String(), nullable = True)
  description=db.Column(db.String(), nullable = True)


class Publisher(db.Model):
  __tablename__ = 'publishers'
  booknum = db.Column(db.String(), nullable = False)
  authornum = db.Column(db.String(), nullable = False)
  publishernum = db.Column(db.String(), nullable = False)
  publisher=db.Column(db.String(), nullable = True)
  parent_company = db.Column(db.String(), nullable = True)
  owner = db.Column(db.String(), nullable = True)
  location = db.Column(db.String(), nullable = True)
  founded = db.Column(db.String(), nullable = True)
  title = db.Column(db.String(), primary_key= True)
  author=db.Column(db.String(), nullable = True)
  wikipedia_page_=db.Column(db.String(), nullable = True)
  description = db.Column(db.String(), nullable = True)
  website = db.Column(db.String(), nullable = True)
  image_url = db.Column(db.String(), nullable = True)
  
db.drop_all()
db.create_all()
# End of models.py