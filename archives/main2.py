#-----------------------------------------
# main2.py
# creating first flask application.
#-----------------------------------------
from flask import Flask, render_template


app = Flask(__name__)

@app.route('/')
def index():
	return render_template('Splash.html')

"""app.route('/Splash/')
def Splash():
	return render_template('Splash.html')"""
@app.route('/book/')
def book():
	return render_template('Titles.html')
@app.route('/Authors/')
def Authors():
	return render_template('Authors.html')
@app.route('/Books/')
def Titles():
	return render_template('Titles.html')
@app.route('/Publishers/')
def Publishers():
	return render_template('Publishers.html')
@app.route('/HarryPotterandtheSorcerersStone/')
def HarryPotterandtheSorcerersStone():
	return render_template('HarryPotterandtheSorcerersStone.html')
@app.route('/JKRowling/')
def JKRowling():
	return render_template('JKRowling.html')
@app.route('/Pottermore/')
def Pottermore():
	return render_template('Pottermore.html')
@app.route('/Foundation/')
def Foundation():
	return render_template('Foundation.html')
@app.route('/IsaacAsimov/')
def IsaacAsimov():
	return render_template('IsaacAsimov.html')
@app.route('/BantamSpectra/')
def BantamSpectra():
	return render_template('BantamSpectra.html')
@app.route('/TheRestaurantattheEndoftheUniverse/')
def TheRestaurantattheEndoftheUniverse():
	return render_template('TheRestaurantattheEndoftheUniverse.html')
@app.route('/DouglasAdams/')
def DouglasAdams():
	return render_template('DouglasAdams.html')
@app.route('/DelReyBooks/')
def DelReyBooks():
	return render_template('DelReyBooks.html')
@app.route('/About/')
def About():
	return render_template('About.html')



if __name__ == "__main__":
	app.run()
#----------------------------------------
# end of main2.py
#-----------------------------------------
